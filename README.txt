
----AUTHOR----
*Harish Naidu (naiduharish)- https://www.drupal.org/u/naiduharish

----DESCRIPTION----
This module allows users to update fields of different nodes of a
content type. There needs to be a common value in the fields of all
nodes which needs to be updated.

----USAGE----
Below is the example scenario of it's working:

* "ContentType" content type has 10 nodes which has same title and body value 
and different images in all the nodes.

* Now if we have to make the changes in body/title then with the help of 
this module we can update all the 10 nodes just by updating one node.

* This will save time of content editor to update content faster.

----INSTALLATION----
The following are the steps to use the module.

* Go to admin/modules page and install the module.

* Go to admin/config/content/node-update-fields page and save the settings.

* Now while adding or editing the node of the content type selected
in the configuration page, you can find a tab below named as 
Update common fields.

* Select the check-box if you wish to update other nodes having 
the same value in the common field.

----Note----
* The nodes will be updated only if this is checked else other nodes would 
remain unaffected.
