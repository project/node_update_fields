<?php

/**
 * @file
 * Common fields administration settings UI.
 */

/**
 * Callback function for admin/config/content/node-update-fields.
 */
function node_update_fields_admin_content($form, &$form_state) {
  $fields = array();
  $options = array();
  $result = '';
  $content_types = node_type_get_types();

  foreach ($content_types as $key => $value) {
    $options[$key] = $value->name;
    $instances = field_info_instances('node', $key);
    $extra_fields = field_info_extra_fields('node', $key, 'form');

    // Fields.
    foreach ($instances as $name => $instance) {
      $field = field_info_field($instance['field_name']);
      if (isset($field['storage']['details']['sql'])) {
        $sql_table[$key][$instance['field_name']] = ($field['storage']['details']['sql']['FIELD_LOAD_CURRENT']);
      }
      $fields[$key][$instance['field_name']] = $instance['label'];
    }
    // Non-field elements.
    foreach ($extra_fields as $name => $extra_field) {
      $fields[$key][$name] = $extra_field['label'];
    }
  }

  variable_set('node_update_fields_field_names', $sql_table);

  $form['content_types'] = array(
    '#type' => 'hidden',
    '#value' => $options,
  );
  $result = variable_get('node_update_fields_update_settings', '');

  foreach ($options as $key => $value) {
    $field_options = array();
    $default_val_settings = '';
    $default_val_common_field = '';
    $default_val_common_fields_to_update = '';
    $start['select'] = t('- Select -');
    $field_options = $start + $fields[$key];
    $collapsed = TRUE;

    if (!empty($result[$key])) {
      $default_val_settings = $result[$key]['apply_settings'];
      $default_val_common_field = $result[$key]['common_field'];
      $default_val_common_fields_to_update = explode(',', $result[$key]['update_fields']);
      $collapsed = FALSE;
    }

    $form['configure-' . $key] = array(
      '#type' => 'fieldset',
      '#title' => t('Common Field settings for :value', array(':value' => $value)),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    );
    $form['configure-' . $key][$key] = array(
      '#type' => 'checkbox',
      '#title' => $value,
      '#description' => t('Apply settings to this content type (<i>If not checked settings will not be applied</i>).'),
      '#default_value' => $default_val_settings,
    );

    $form['configure-' . $key]['common_' . $key] = array(
      '#type' => 'select',
      '#title' => t('Common field'),
      '#options' => $field_options,
      '#default_value' => $default_val_common_field,
      '#description' => t('Select the common field with same value among the content type.'),
    );

    $form['configure-' . $key]['all_' . $key] = array(
      '#type' => 'select',
      '#title' => t('Fields to update'),
      '#options' => $field_options,
      '#multiple' => TRUE,
      '#default_value' => $default_val_common_fields_to_update,
      '#description' => t('Select common node fields to be updated (<i>This field should not be same as common field</i>).'),
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit Handler for the node_update_fields_admin_content form.
 */
function node_update_fields_admin_content_submit($form, &$form_state) {
  $values = $form_state['values'];
  $options = $form_state['values']['content_types'];

  switch ($values['op']) {
    case 'Save':
      $variable_array = array();

      foreach ($values as $key => $value) {
        if (array_key_exists($key, $options) && $value == 1) {
          $variable_array[$key]['content_type'] = $key;
          $variable_array[$key]['apply_settings'] = $values[$key];
          $variable_array[$key]['common_field'] = $values['common_' . $key];
          $common_fields_to_update = '';

          if (!empty($values['all_' . $key])) {
            foreach ($values['all_' . $key] as $field_name) {
              $common_fields_to_update .= $field_name . ',';
            }
          }
          $variable_array[$key]['update_fields'] = substr($common_fields_to_update, 0, -1);
        }
      }
      variable_set('node_update_fields_update_settings', $variable_array);
  }
  drupal_set_message(t('The settings have been saved'));
}
